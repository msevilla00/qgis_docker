# Docker image to run QGIS Long Term Release
# 230421 - CC BY-SA - Miguel Sevilla-Callejo
# QGIS 3.28.5

FROM debian:bookworm

LABEL Name=qgis_ltr Version=240227
LABEL maintainer Miguel Sevilla-Callejo "https://msevilla00.gitlab.io"

ENV DEBIAN_FRONTEND noninteractive

# default user: `qgis`
#ENV USER="qgis"

# add necessary packages
RUN apt-get update \
  && apt-get install -y \
    apt-utils \
    curl \
    gpg \
    keyboard-configuration \
    nano \
    software-properties-common \
    supervisor \
    ssh \
    unzip \
    wget \
    xvfb

#ENV LANG en_US.UTF-8
#ENV LANGUAGE en_US.UTF-8
#ENV LC_ALL en_US.UTF-8

# ADD QGIS to the system ######################################################## 

# Add QGIS Long Term Release repository
RUN echo -e "Types: deb deb-srcURIs: https://qgis.org/debian\nSuites: bookworm\nArchitectures: amd64\nComponents: main\nSigned-By: /etc/apt/keyrings/qgis-archive-keyring.gpg" > /etc/apt/sources.list.d/qgis.sources

# Add QGIS repository
#RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-key 51F523511C7028C3
#RUN apt install -y gnupg software-properties-common
RUN mkdir -m755 -p /etc/apt/keyrings
RUN wget -O /etc/apt/keyrings/qgis-archive-keyring.gpg https://download.qgis.org/downloads/qgis-archive-keyring.gpg
RUN apt-get update
RUN apt-get install -y lxqt-core
RUN apt-get install -y qgis qgis-plugin-grass 

# Uncomment if you want to install QGIS server
#RUN apt-get install qgis-server

# Uncomment if you want to install GRASS & SAGA
RUN apt-get install -y grass saga

# Set up SSH service to use X outside of the container
RUN sed -i 's/\#X11UseLocalhost yes/X11UseLocalhost no/g' /etc/ssh/sshd_config

# Add supervisor ##########################################################

# create directory for child images to store configuration in
RUN mkdir -p /var/log/supervisor

    # Add supervisor service configuration script
COPY ./supervisor/ /etc/supervisor

    # Run supervisor
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]


###########################################################################
# To BUILD IMAGE with this code run:
# `docker build -t msevilla00/qgis:ltr -f qgis_ltr_bookworm.dockerfile .`

# To RUN QGIS from container:
# xhost +
# docker run --rm -it --name qgis_ltr \
#   -v /datafolder:/root/ -v /tmp/.X11-unix:/tmp/.X11-unix \
#   -e DISPLAY=unix$DISPLAY msevilla00/qgis:ltr qgis