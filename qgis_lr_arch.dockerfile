# Docker image to run QGIS Latest Release
# 20220205 - CC BY-SA - Miguel Sevilla-Callejo

FROM archlinux:latest

LABEL Name=qgis_latest Version=0.1
LABEL maintainer Miguel Sevilla-Callejo "https://msevilla00.gitlab.io"

#ENV DEBIAN_FRONTEND noninteractive

# default user: `qgis`
#ENV USER="qgis"

# create an lsign-key
# https://hub.docker.com/_/archlinux?tab=description
RUN pacman-key --init

# add necessary packages
RUN pacman -Syu --noconfirm \
    && pacman -S --noconfirm \
    vim \
    supervisor \
    openssh \
    unzip \
    xorg-server-xvfb \
    qgis

#ENV LANG en_US.UTF-8
#ENV LANGUAGE en_US.UTF-8
#ENV LC_ALL en_US.UTF-8

RUN pacman -S --noconfirm lxqt

#RUN yay -S --noconfirm grass saga
#RUN yay -S --noconfirm qgis-plugin-grass 
#RUN yay -S --noconfirm qgis-server

# Set up SSH service to use X outside of the container
RUN sed -i 's/\#X11UseLocalhost yes/X11UseLocalhost no/g' /etc/ssh/sshd_config

# Add supervisor ##########################################################

# create directory for child images to store configuration in
RUN mkdir -p /var/log/supervisor

    # Add supervisor service configuration script
COPY ./supervisor/ /etc/supervisor

    # Run supervisor
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]


###########################################################################
# To BUILD AN IMAGE with this code run:
# `docker build -t msevilla00/qgis:arch_lr -f qgis_lr_arch.dockerfile .`

# To RUN QGIS from container:
# xhost + 
# & docker run --rm -it --name qgis_latest \
#   -v /datafolder:/root/ -v /tmp/.X11-unix:/tmp/.X11-unix \
#   -e DISPLAY=unix$DISPLAY msevilla00/qgis:arch_lr qgis

# NO FUNCIONA al lanzar QGIS por supervisor
# REVISAR por ssh